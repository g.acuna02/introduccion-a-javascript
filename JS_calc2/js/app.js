var inputOne = document.getElementById("first-input");
var inputTwo = document.getElementById("second-input");

var button_solve = document.getElementById("solve");
var button_clean = document.getElementById("clean");

var button_multiply = document.getElementById("multiply");
var button_division = document.getElementById("division");
var button_minus = document.getElementById("minus");
var button_plus = document.getElementById("plus");

var result = document.getElementById("result");

var operation = null;

button_plus.onclick = function (e){
    operation = "+";
    cleanButtonStyles();
    button_plus.classList= "highlight_button"
}
button_minus.onclick = function (e){
    operation = "-";
    cleanButtonStyles();
    button_minus.classList= "highlight_button"
}
button_multiply.onclick = function (e){
    operation = "*";
    cleanButtonStyles();
    button_multiply.classList= "highlight_button"
}
button_division.onclick = function (e){
    operation = "/"
    cleanButtonStyles();
    button_division.classList= "highlight_button"
}

button_solve.onclick = function (e){
    solve();
}
button_clean.onclick = function (e){
    clean();
}

function clean(){
    inputOne.value = 0;
    inputTwo.value = 0;
    operation = "";
    cleanButtonStyles();
}

function solve() {
    if(!validate(false)){
        return;
    } 
    var inputOne_value = inputOne.value;
    var inputTwo_value = inputTwo.value;
    var res = 0;
    switch (operation) {
        case "+":
            res = parseFloat(inputOne_value) + parseFloat(inputTwo_value);
            break;
        case "-":
            res = parseFloat(inputOne_value) - parseFloat(inputTwo_value);
            break;
        case "*":
            res = parseFloat(inputOne_value) * parseFloat(inputTwo_value);
            break;
        case "/":
            if(!validate(true)){
                return;
            } 
            res = parseFloat(inputOne_value) / parseFloat(inputTwo_value);
            break;
        default:
            res = "Debes indicar una operación";
    }
    result.textContent = res;
    result.style = "color: black;";
}

function cleanButtonStyles () {
    button_plus.classList= "button";
    button_minus.classList= "button";
    button_multiply.classList= "button";
    button_division.classList= "button";
}

function validate(is_division){
    if (!inputOne.value || !inputTwo.value){
        result.textContent = "No puede dejar vacio una celda";
        result.style = "color: red;";
        return false;
    }
    if (inputOne.value.includes(".") || inputOne.value.includes(",") || inputTwo.value.includes(".") || inputTwo.value.includes(",")){
        result.textContent = "Solo numeros enteros";
        result.style = "color: red;";
        return false;
    }
    if(is_division && inputTwo.value == 0){
        result.textContent = "No se puede dividir por 0";
        result.style = "color: red;";
        return false;
    }
    
    return true;
}

